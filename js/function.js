
jQuery(window).load(function(){

$(".loader").fadeOut('slow');
jQuery('body').delay(350).css({'overflow':'visible'});

});

jQuery(function($) {'use strict',
	"use strict";

// ============= Initiat WOW JS ================
	new WOW().init();


// ============= Revolution Slider =============

var revapi;
		revapi = jQuery("#rev_slider_3").revolution({
			sliderType:"standard",
			sliderLayout:"fullwidth",
			hide_under:778,
			delay:5000,
			navigation: {
				arrows:{enable:true}				
			},
			touch:{
			 touchenabled:"on",
			 swipe_threshold: 75,
			 swipe_min_touches: 1,
			 swipe_direction: "horizontal",
			 drag_block_vertical: false
		 },			
			gridwidth:1170,
			gridheight:920		
});	

// ============= Carousel =============

// For Testinomials
$("#testimonial-slider").owlCarousel({
		autoPlay: 3000, 
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		pagination:true,
		paginationNumbers:false,
 
 });
 // For Latest News
$("#latest_news-slider_1").owlCarousel({
		autoPlay: 3000, 
		items : 3,
		slideSpeed : 300,
		paginationSpeed : 400,
		paginationNumbers:false,
		dots:true,
 
 });
          

});







